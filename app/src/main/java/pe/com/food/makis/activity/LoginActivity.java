package pe.com.food.makis.activity;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import pe.com.food.makis.R;
import pe.com.food.makis.util.FunctionsUtil;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputLayout tinPassword;
    TextInputLayout tinUser;

    EditText eteUser;
    EditText etePassword;
    Button butLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tinPassword = findViewById(R.id.tinPassword);
        tinUser = findViewById(R.id.tinUser);
        eteUser = findViewById(R.id.eteUser);
        etePassword = findViewById(R.id.etePassword);
        butLogin = findViewById(R.id.butLogin);

        butLogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.butLogin:
                if (isValid()) {

                }


                break;
            default:
                break;
        }
    }


    public boolean isValid() {

        boolean isValid = FunctionsUtil.emptyValidate(eteUser, getResources().getString(R.string.login_error));

        if (isValid)
            isValid = FunctionsUtil.emptyValidate(etePassword, getResources().getString(R.string.login_error_password));


        return isValid;

    }

}
