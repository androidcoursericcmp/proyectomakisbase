package pe.com.food.makis.util;


import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;

import java.io.IOException;
import java.io.InputStream;


public class FunctionsUtil {



    /**
     * Loads content of file from assets as string.
     */
    public static String loadJSONFromAsset(Context context, String name) {
        String json = "";
        try {
            InputStream is = context.getAssets().open(name);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public static boolean emptyValidate(EditText txt, String error) {
        txt.setError(null);
        txt.requestFocus();
        if (TextUtils.isEmpty(txt.getText())) {
            txt.setError(error);
            return false;
        }
        return true;
    }

}
